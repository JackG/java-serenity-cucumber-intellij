package com.j4soft.bdd.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SampleFeatureSteps {

    @When("^this sample feature is launched$")
    public void thisSampleFeatureIsLaunched() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^wonders happen$")
    public void wondersHappen() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
