package com.j4soft.bdd.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AnotherFeatureSteps {

    @When("^this other feature is launched$")
    public void thisOtherFeatureIsLaunched() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^bad things can happen$")
    public void badThingsCanHappen() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
